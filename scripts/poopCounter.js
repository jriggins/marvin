var date = new Date()

var moment = require('moment')

function millisecondsToSomethingUseful(ms) {
  var d = moment.duration(ms, 'milliseconds')
  var days = Math.floor(d.asDays())
  var hours = Math.floor(d.asHours()) - days * 24
  var minutes = Math.floor(d.asMinutes()) - (hours * 60) - (days * 24 *60)
  var seconds = Math.floor(d.asSeconds()) - (minutes * 60) - (hours * 60 * 60) - (days * 24 * 60 * 60)

  return days + ' day(s), ' + hours + ' hour(s), ' + minutes + ' minute(s), and ' + seconds + ' second(s)'
}

module.exports = function (robot) {
  setTimeout(function() {
    var storedBest = robot.brain.get('best')
    var storedDate = robot.brain.get('date')

    console.log('best: ' + storedBest)
    console.log('date: ' + storedDate)
  }, 2000)

  robot.hear(/check yo/, function (msg) {
    msg.send('best: ' + storedBest)
    msg.send('date: ' + storedDate)
  })

  robot.hear(/[pP]oop( counter)?/, function(msg) {
    var currentDate = new Date()
    var prevDate = new Date(robot.brain.get('date') || date)
    var diff = (currentDate - prevDate)

    if (msg.match[1]) {
      msg.send("The last time you humans talked about poop was " + moment(prevDate).from(currentDate))
      msg.send("That was " + millisecondsToSomethingUseful(diff) + " ago.")
    } else {
      msg.emote(":poop: Resetting the poop counter.")

      msg.send("You humans made it " + millisecondsToSomethingUseful(diff) + " without talking about poop.")

      var best = robot.brain.get('best') || 0
      if (best >= diff) {
        msg.send("Your previous best was " + millisecondsToSomethingUseful(best) + ".")
      } else {
        if (best !== 0) msg.send("That was your best stretch yet.")

        robot.brain.set('best', diff)
      }

      robot.brain.set('date', currentDate)
    }
  })
}

